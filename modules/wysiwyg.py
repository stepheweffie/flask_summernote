from flask import request
import shutil
import os
from werkzeug.utils import secure_filename


def handle_form_data(form):

    # https://stackoverflow.com/questions/10434599/get-the-data-received-in-a-flask-request
    print(request.form)
    print(request.values)
    print(request.args)
    print(request.get_data())
    file_path0 = os.environ['HTML_UPLOAD_DIRECTORY']
    print(file_path0)
    # TODO allow subdirectory in form
    # TODO form in Helm settings to change HTML_UPLOAD_DIRECTORY
    try:
        os.mkdir(file_path0)
    except OSError:
        pass
    filename = request.form.get('title')
    print(form['title'], filename)
    file = request.form.get('file')
    print(file)

    # should print a beautiful HTML string as the value of 'file' in the Dict
    if form['title'] == '':
        # TODO ask user for file name
        filename = 'please_name_this_file.html'
    else:
        filename = form['title'] + '.html'
    if len(file) > 0:
        file = file
    if file and filename:
        filename = secure_filename(filename)
        print(filename)

        with open(filename, 'w') as f:
            f.write(file)
        f.close()

        file_path0 = file_path0 + '/%s' % filename
        file_path = os.getcwd()
        file_path = file_path + '/%s' % filename
        shutil.copy(file_path, file_path0)
        shutil.move(file_path, file_path0)