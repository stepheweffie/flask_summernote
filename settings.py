import dotenv
import os
from os import getenv
from flask import current_app as app
# Use dotenv set in terminal to avoid editing .env file

# https://github.com/theskumar/python-dotenv#readme


def dot_env():
    dotenv.load_dotenv(
    os.path.join(os.path.dirname(os.path.dirname(__file__)), '.env'))




