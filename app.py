from flask import Flask, url_for, render_template, redirect, request, flash, send_from_directory
from flask.views import MethodView
from settings import dot_env
import os
from wtforms.widgets import HTMLString
import time
from flask_admin import Admin
import os.path as op
from flask_admin.contrib.fileadmin import FileAdmin
from flask_admin.menu import MenuLink
from flask_admin import expose, BaseView
from flask_admin.form.upload import ImageUploadInput
from modules.wysiwyg import handle_form_data

app = Flask(__name__)
# another way to add environment vars is to update the .json
app.config.update(
        DEBUG=True,
        TESTING=True,
    )
dot_env()
app.secret_key = os.getenv('SECRET_KEY')


class SiteIndex(MethodView):
    def __init__(self, posts=list, posted=dict):
        self.posts = posts
        self.posted = posted

    def get(self):
        alt_text = "There are several bootswatch themes available in the /admin/swatches"
        alt_text_error = "Key Error"
        try:
            swatch = os.environ['FLASK_ADMIN_SWATCH']
            if swatch:
                return render_template('site/index.html')
            else:
                return alt_text
        except KeyError:
            return alt_text_error

    def post(self):
        # posts.append(posted)
        return render_template('site/index.html',  posts=self.posts)

    def put(self):
        return render_template('site/index.html', posts=self.posts)

    def delete(self):
        return render_template('site/index.html', posts=self.posts)


app.add_url_rule('/', view_func=SiteIndex(posts=list, posted=dict).as_view('/'))

"""Admin Menu Views Here"""


class ContentView(BaseView):
    @expose('/', methods=["GET", "POST"])
    def index(self):
        if request.method == "GET":
            return self.render('/admin/content.html')
        else:
            pass


class PreviewView(BaseView):
    @expose('/', methods=['GET', "POST"])
    def index(self):
        if request.method == "GET":
            return self.render('/admin/preview.html')
        else:
            form = request.form
            handle_form_data(form)
            if request.form.get("save"):
                return redirect(url_for('fileadmin.index_view'))
            elif request.form.get("add_another"):
                # TODO make flash message more secure
                flash("Your file was saved in %s" % os.environ['HTML_UPLOAD_DIRECTORY'])
                return self.render('/admin/preview.html')
                # TODO automate silent saving in background
            else:
                return 'Click back button to return to the form' + '<br>' + form["title"] + '<br>' + form["file"]


class DashboardView(BaseView):
    @expose('/', methods=["GET", "POST"])
    # TODO add settings form
    def index(self):
        return self.render('/admin/dashboard.html')


"""Individual File Views Here"""


@app.route('/content/<filename>')
# https://flask.palletsprojects.com/en/1.1.x/patterns/fileuploads/
def html_content_file(filename):
    return send_from_directory(os.environ["HTML_UPLOAD_DIRECTORY"], filename)


@app.route('/content/image/<filename>')
def image_content_file(filename):
    return send_from_directory(os.environ['IMAGE_UPLOAD_DIRECTORY'], filename)


path = op.join(op.dirname(__file__), 'templates/content')
admin = Admin(app, template_mode='bootstrap3')
admin.add_view(ContentView(name="Publish", endpoint="publish_content"))
admin.add_view(PreviewView(name="Compose", endpoint="compose"))
admin.add_view(DashboardView(name="Helm", endpoint="helm"))
admin.add_view(FileAdmin(base_path=path))
admin.add_link(MenuLink(name="Exit Admin", category="", url='/'))

if __name__ == '__main__':
    app.run()






