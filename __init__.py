from flask import Flask
from settings import dot_env

app = Flask(__name__)
# another way to add environment vars is to update the .json
app.config.update(
        DEBUG=True,
        TESTING=True,
    )
dot_env()

if __name__ == '__main__':
    app.run()